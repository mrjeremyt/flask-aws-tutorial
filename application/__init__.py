from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
#import pymysql
#pymysql.install_as_MySQLdb()

application = Flask(__name__)
application.config.from_object('config')
db = SQLAlchemy(application)
